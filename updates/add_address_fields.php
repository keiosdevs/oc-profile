<?php namespace Keios\PrintSeller\Updates;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use October\Rain\Database\Updates\Migration;

class AddAddressFields extends Migration
{

    public function up()
    {
        if (!Schema::hasColumn('keios_prouser_users', 'address')) {
            Schema::table(
                'keios_prouser_users',
                function (Blueprint $table) {
                    $table->string('address')->after('register_number')->nullable();
                    $table->string('address2')->after('address')->nullable();
                }
            );
        }
    }

    public function down()
    {
        Schema::table(
            'keios_prouser_users',
            function (Blueprint $table) {
//                $table->dropColumn('is_company');
//                $table->dropColumn('wants_invoice');
//                $table->dropColumn('company_name');
//                $table->dropColumn('vat_number');
//                $table->dropColumn('register_number');
//                $table->dropColumn('street');
//                $table->dropColumn('house_number');
//                $table->dropColumn('flat_number');
//                $table->dropColumn('zip');
//                $table->dropColumn('city');
//                $table->dropColumn('phone');
            }
        );
    }

}
