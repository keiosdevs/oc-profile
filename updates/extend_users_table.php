<?php namespace Keios\PrintSeller\Updates;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use October\Rain\Database\Updates\Migration;

class ExtendUsersTable extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_prouser_users',
            function (Blueprint $table) {
                $table->boolean('is_company')->default(false);
                $table->boolean('wants_invoice')->default(false);
                $table->string('company_name')->nullable();
                $table->string('vat_number')->nullable();
                $table->string('register_number')->nullable();
                $table->string('street')->nullable();
                $table->char('house_number', 8)->nullable();
                $table->char('flat_number', 8)->nullable();
                $table->char('zip', 8)->nullable();
                $table->string('city')->nullable();
                $table->string('phone')->nullable();

            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_prouser_users',
            function (Blueprint $table) {
//                $table->dropColumn('is_company');
//                $table->dropColumn('wants_invoice');
//                $table->dropColumn('company_name');
//                $table->dropColumn('vat_number');
//                $table->dropColumn('register_number');
//                $table->dropColumn('street');
//                $table->dropColumn('house_number');
//                $table->dropColumn('flat_number');
//                $table->dropColumn('zip');
//                $table->dropColumn('city');
//                $table->dropColumn('phone');
            }
        );
    }

}
