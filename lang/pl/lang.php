<?php

return [
    'profile' => [
        'firstName' => 'Imię',
        'lastName' => 'Nazwisko',
        'street' => 'Ulica',
        'house_number' => 'Nr domu',
        'flat_number' => 'Nr lokalu',
        'zip' => 'Kod pocztowy',
        'city' => 'Miejscowość',
        'phone' => 'Nr telefonu',
        'nip' => 'NIP',
        'wantsInvoice' => 'Fakturowany',
        'isCompany' => 'Jest firmą',
        'companyName' => 'Nazwa firmy',
        'register' => 'KRS',
    ],
];