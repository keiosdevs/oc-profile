<?php

return [
    'profile' => [
        'firstName' => 'First name',
        'lastName' => 'Last name',
        'street' => 'Street',
        'house_number' => 'House no',
        'flat_number' => 'Flat no',
        'zip' => 'ZIP code',
        'city' => 'City',
        'phone' => 'Phone no',
        'nip' => 'VAT no',
        'companyName' => 'Company name',
        'isCompany' => 'Is company',
        'wantsInvoice' => 'Wants invoice',
        'register' => 'Company register number',
        'address' => 'Address',
        'address2' => 'Address (2)'
    ],
];