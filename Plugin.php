<?php namespace Keios\Profile;

use Backend\Widgets\Form;
use Keios\ProUser\Controllers\Users as UsersController;
use Keios\ProUser\Models\User;
use System\Classes\PluginBase;

/**
 * Profile Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['Keios.ProUser'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'User Profile',
            'description' => 'Provides ProUser plugin profiles',
            'author'      => 'Keios',
            'icon'        => 'icon-user'
        ];
    }

    public function register()
    {
        User::extend(
            function (User $user) {
                $user->addFillable(
                    [
                        'is_company',
                        'wants_invoice',
                        'street',
                        'address',
                        'address2',
                        'house_number',
                        'flat_number',
                        'zip',
                        'city',
                        'phone',
                        'vat_number',
                        'register_number',
                        'company_name',
                    ]
                );
            }
        );

        UsersController::extendFormFields(
            function (Form $form, $model, $context) {
                if (!$model instanceof User) {
                    return;
                }

                $form->addTabFields(
                    [
                        'is_company'      => [
                            'label' => 'keios.profile::lang.profile.isCompany',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'checkbox',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-6'
                        ],
                        'wants_invoice'   => [
                            'label' => 'keios.profile::lang.profile.wantsInvoice',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'checkbox',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-6'
                        ],
                        'address'          => [
                            'label' => 'keios.profile::lang.profile.address',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-12'
                        ],
                        'address2'          => [
                            'label' => 'keios.profile::lang.profile.address2',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-12'
                        ],
                        'street'          => [
                            'label' => 'keios.profile::lang.profile.street',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-8'
                        ],
                        'house_number'    => [
                            'label' => 'keios.profile::lang.profile.house_number',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'span'  => 'none',
                            'type' => 'text',
                            'cssClass' => 'col-sm-2'
                        ],
                        'flat_number'     => [
                            'label' => 'keios.profile::lang.profile.flat_number',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-2'
                        ],
                        'zip'             => [
                            'label' => 'keios.profile::lang.profile.zip',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-4'
                        ],
                        'city'            => [
                            'label' => 'keios.profile::lang.profile.city',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-8'
                        ],
                        'phone'           => [
                            'label' => 'keios.profile::lang.profile.phone',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-12'
                        ],
                        'vat_number'      => [
                            'label' => 'keios.profile::lang.profile.nip',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-6'
                        ],
                        'register_number' => [
                            'label' => 'keios.profile::lang.profile.register',
                            'tab'   => 'keios.prouser::lang.fields.profile',
                            'type'  => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-6'
                        ],
                        'company_name'    => [
                            'label'   => 'keios.profile::lang.profile.companyName',
                            'tab'     => 'keios.prouser::lang.fields.profile',
                            'type'    => 'text',
                            'span'  => 'none',
                            'cssClass' => 'col-sm-12',
                            'trigger' => [
                                'action'    => 'show',
                                'field'     => 'is_company',
                                'condition' => 'checked'
                            ]
                        ],
                    ]
                );
            }
        );
    }
}
